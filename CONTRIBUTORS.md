Contributors.md
=================

We tried to delineate who contributed what to the best of our ability but the majority of the files we pair programmed in the same room together. So we are attributing some of the major files to both of us. 

James Marvin (<jmarvin1@nd.edu>)
================================

* Pair programmed: haxtable.py, bench.py, test.py 
* Led haxlife.py
* Wrote README.md
* Wrote some of the slides for presentation
* Testing and bug fixes 

J. Patrick Lacher (<jlacher1@nd.edu>)
=====================================

* Pair programmed: haxtable.py, bench.py, test.py
* Contributed to haxlife.py
* Wrote shapwd.py
* Configured lifehaxlib as a python package
* Deployed CI on GitLab
* Wrote some of the slides for presentation
* Testing and bugfixes
