__**LifeHax**__
=======

About
-----

LifeHax has been developed as part of the Fall 2016 Data Structures (CSE-30331-FA16) course at the University of Notre Dame. The goal of this project is to create a encrypted password manager. The manager allows multiple users to store multiple passwords and hide their information from would be attackers. We store our user information in a shelf of open addressed hash tables. The data is encrypted using the Vigenère cipher, which Caesar shifts every character separately based on a provided key. 


Requirements/Instalation
-------------------------

First, you need to clone our repository. After that you must run the setup.py script to install the lifhaxlib package. 

	$sudo python setup.py install
	
The library and front end use a number of python packages from the standard python library so no additional packages are required. 

Our example front end is included in src/haxlife.py.

	$python src/haxlife.py
	
The lifehaxlib package has been configured for use with any other program in python by importing like a typical package once installed. 


Developers
----------

* J. Patrick Lacher (<jlacher1@nd.edu>)
* James Marvin (<jmarvin1@nd.edu>)
