import os
import sys
import lifehaxlib
import time
import random

ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

if os.path.exists('lifehax'): os.system('rm lifehax*')
#insertion
start_time = time.time()
t = lifehaxlib.haxtable.table()
#store all usernames
users = []
for j in range(int(sys.argv[1])):
	user = ''.join(random.choice(ALPHABET) for i in range(10))
	key = ''.join(random.choice(ALPHABET) for i in range(10))
	t.insert('testuser','testpass',user,key)
	users.append(user)
	
print 'insert time: {} seconds'.format(time.time()-start_time)

#lookup random particular password
target = random.choice(users)
start_time = time.time()
listing = t.getPasswords('testuser','testpass')
print 'all passwords lookup time: {} seconds'.format(time.time()-start_time)

for entry in listing:
	if entry[0]==target:
		break

print 'particular lookup time: {} seconds'.format(time.time()-start_time)



