import lifehaxlib
import sys
import os
import random

#remove old table, if it exists-----------------------------------------------------
if os.path.exists('lifehax'): os.system('rm lifehax*')

#create table-----------------------------------------------------------------------
t = lifehaxlib.haxtable.table()

#insert a number of elements into one user------------------------------------------
print "Inserting elements for one user..."
try:
	t.insert("jmarvin1","testpass","jmarvu1","jmarvp1")
	t.insert("jmarvin1","testpass","jmarvu2","jmarvp2")
	t.insert("jmarvin1","testpass","jmarvu3","jmarvp3")
except:
	print >> sys.stderr, "Error inserting elements into single user"
	sys.exit(1)
	
#lookup insertions from one user----------------------------------------------------
print "Retrieving elements for one user..."
try:
	values = t.getPasswords("jmarvin1","testpass")
except:
	print >> sys.stderr, "Error retrieving elements from a single user"
	sys.exit(1)
	
#validate that returned values are all stored values--------------------------------
print "Validating elements for one user..."
test = [False, False, False]
for entry in values:
	if entry[0] == "jmarvu1" and entry[1] == "jmarvp1":
		test[0] = True
	elif entry[0] == "jmarvu2" and entry[1] == "jmarvp2":
		test[1] = True
	elif entry[0] == "jmarvu3" and entry[1] == "jmarvp3":
		test[2] = True
if (False) in test:
	print >> sys.stderr, "Error validating elements from single user"
	sys.exit(1)
	
#validate persistence---------------------------------------------------------------
print "Validating persistence..."
del t
nt = lifehaxlib.haxtable.table()
nvalues = nt.getPasswords("jmarvin1","testpass")
for entry in values:
	found = False
	for nentry in nvalues:
		if entry[0] == nentry[0] and entry[1] == nentry[1]:
			found = True
			break
	if not found:
		print >> sys.stderr, "Error in persistence"
		sys.exit(1)
	
#validate that second user input does not affect first user-------------------------
print "Ensuring second user does not affect first..."
nt.insert("jlacher1","testpass2","jlachu1","jlachp1")
nnvalues = nt.getPasswords("jmarvin1","testpass")
for entry in values:
	found = False
	for nnentry in nnvalues:
		if entry[0] == nnentry[0] and entry[1] == nnentry[1]:
			found = True
			break
	if not found:
		print >> sys.stderr, "Error differentiating users"
		sys.exit(1)
		
#ensure table resizes correctly-----------------------------------------------------
ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
print "Ensuring open-addressed tables resize properly..."
try:
	for insertion in range(200):
		user = ''.join(random.choice(ALPHABET) for i in range(10))
		key = ''.join(random.choice(ALPHABET) for i in range(10))
		nt.insert('testuser','testpass',user,key)
except:
	print >> sys.stderr, "Error in open-addressing table resizing..."
	sys.exit(1)
	
#cleanup----------------------------------------------------------------------------
os.system('rm lifehax*')