from distutils.core import setup

setup(name="lifehaxlib",
		description="Persistent Secure Password Database",
		author="J. Patrick Lacher and James Marvin",
		author_email="jlacher1@nd.edu",
		url="https://gitlab.com/placher/lifehax",
		packages=["lifehaxlib"],
		package_dir={'':'src'})