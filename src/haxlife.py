'''
haxlife.py
Authors: J. Patrick Lacher and James Marvin

This program is a persistent password manager similar to Apple's keychain
that uses the custom python library lifehaxlib to implement the database.

You may need to run the setup.py script included in the LifeHax repository for
this program to function properly.
'''
#Imports
#-------------------------------------------------------------------------------------------------------------------------------
import lifehaxlib
import os
import sys

#Function Definitions
#-------------------------------------------------------------------------------------------------------------------------------
def printHeader():
	'''Prints the haxlife header'''
	print '       ___           ___           __                     ___                                  ___     '
	print '      /  /\         /  /\         |  |\                  /  /\       ___         ___          /  /\    '
	print '     /  /:/        /  /::\        |  |:|                /  /:/      /__/\       /  /\        /  /::\   '
	print '    /  /:/        /  /:/\:\       |  |:|               /  /:/       \__\:\     /  /::\      /  /:/\:\  '
	print '   /  /::\ ___   /  /::\ \:\      |__|:|__            /  /:/        /  /::\   /  /:/\:\    /  /::\ \:\ '
	print '  /__/:/\:\  /\ /__/:/\:\_\:\ ____/__/::::\          /__/:/      __/  /:/\/  /  /::\ \:\  /__/:/\:\ \:\ '
	print '  \__\/  \:\/:/ \__\/  \:\/:/ \__\::::/~~~~          \  \:\     /__/\/:/~~  /__/:/\:\ \:\ \  \:\ \:\_\/ '
	print '       \__\::/       \__\::/     |~~|:|               \  \:\    \  \::/     \__\/  \:\_\/  \  \:\ \:\  '
	print '       /  /:/        /  /:/      |  |:|                \  \:\    \  \:\          \  \:\     \  \:\_\/  '
	print '      /__/:/        /__/:/       |__|:|                 \  \:\    \__\/           \__\/      \  \:\    '
	print '      \__\/         \__\/         \__\|                  \__\/                                \__\/    '
	print '\n=======================================================================================================\n\n\n'
	
def getPass(user,password):
	'''Opens the table and prints the username/password combinations for the passed user'''
	#get user input
	os.system('clear')
	printHeader()
	print 'All stored data for ' + user + ':\n'
	#read database
	t = lifehaxlib.haxtable.table()
	listing = t.getPasswords(user,password)
	if len(listing) == 0:
		#no data found for master username
		print 'No data found for ' + user
	else:
		#print all available information
		for entry in listing:
			print 'username: ' + entry[0] + '\npassword: ' + entry[1] + '\n'
	pause = raw_input('Press Enter to Continue...')
	
def getParticularPass(user,password):
	'''Search the given user's database for a particular entry'''
	#get user input
	os.system('clear')
	printHeader()
	target = raw_input('Enter the domain you are searching for: ')
	#read database
	t = lifehaxlib.haxtable.table()
	listing = t.getPasswords(user,password)
	found = False
	#loop over entries to find the target record
	for entry in listing:
		if entry[0]==target:
			#target entry found
			print 'username: ' + entry[0] + '\npassword: ' + entry[1]
			print ''
			found = True
	if not found:
		#unable to find target record
		print 'Unable to find data for that domain. You messed up A-Aron'
	pause = raw_input('Press Enter to Continue...')

def addPass(username,password):
	'''Add a new entry to the database for a particular user'''
	#get user input
	os.system('clear')
	printHeader()
	print 'Enter a new domain and password to be stored in the database.\nIf you would like the system to generate a secure password for you, enter nothing in the password field'
	print ''
	new_user = raw_input('Enter new username: ')
	new_pass = raw_input('Enter new password: ')
	#create secure password if user doesn't specify one
	if new_pass == '':
		new_pass = lifehaxlib.shapwd.shapwd(new_user)
	#open database and insert entry
	t = lifehaxlib.haxtable.table()
	t.insert(username,password,new_user,new_pass)
	pause = raw_input('Database Updated.\nPress Enter to Continue...')
	
def performTasks(user,passwd):
	'''Perform a single task on the database'''
	#get user input
	os.system('clear')
	printHeader()
	print 'Choose from the following:\n'
	print '1. Get All Your Stored Passwords'
	print '2. Search for a Particular Password'
	print '3. Insert a New Password Into Your Database'
	print '4. Exit Program\n'
	choice = raw_input('Operation: ')
	#validate input
	valid = ['1','2','3','4']
	if choice not in valid:
		while choice not in valid:
			print 'Invalid input! Please enter 1, 2, 3, or 4'
			choice = raw_input('Operation: ')
	#parse input to proper handler function
	if choice == '1':
		#retrieve all passwords from database and continue program
		getPass(user,passwd)
		return False
	elif choice == '2':
		#retrieve specific password from database and continue program
		getParticularPass(user,passwd)
		return False
	elif choice == '3':
		#add password to database and continue program
		addPass(user,passwd)
		return False
	elif choice == '4':
		#quit program
		return True
		
def exitProgram():
	'''Gracefully prints closing message and ends the program'''
	os.system('clear')
	printHeader()
	print 'Thank you for using the HaxLife Password Manager'
	print 'Your database has been saved'
	print 'You can access it by entering the same login information next time'
	print '\nHave a nice day!\n'
	sys.exit(0)
	
#Start of Main Execution 
#-------------------------------------------------------------------------------------------------------------------------------
os.system('clear')
printHeader()
print 'Welcome to the HaxLife Password Manager!\n'
#get user information
print 'Please enter your master username and password to get started'
user = raw_input('Username: ')
passwd = raw_input('Password: ')
#validate user information
t = lifehaxlib.haxtable.table()
if not t.validateUser(user,passwd):
	while not t.validateUser(user,passwd):
		os.system.clear('clear')
		printHeader()
		print 'Your login information was incorrect. Please try again:\n'
		user = raw_input('Username: ')
		passwd = raw_input('Password: ')
quit = False
#offer functionality
try:
	while not quit:
		quit = performTasks(user,passwd)
	exitProgram()
except KeyboardInterrupt as e:
	#interpret ^C and gracefully end the program
	exitProgram()



			

