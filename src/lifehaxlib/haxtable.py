class table(object):

	def __init__(self,filename='lifehax'):
		self.filename=filename
	
	def insert(self,muser,mpass,username,password):
		'''insert a new username/password combination into muser's folder'''
		import shelve
		import vigenere
		#get persistent database information
		users = shelve.open(self.filename,writeback=True)
		if muser in users:
			#existing user
			users[muser].insert(vigenere.encode(mpass,username),vigenere.encode(mpass,password))
		else:
			#new user
			users[muser] = childHashTable()
			users[muser].insert(vigenere.encode(mpass,'validuser'),vigenere.encode(mpass,'validated'))
			users[muser].insert(vigenere.encode(mpass,username),vigenere.encode(mpass,password))
		users.close()	
	
	def getPasswords(self,muser,mpass):
		'''decode and return a listing of muser's username/password combinations'''
		import shelve
		import vigenere
		#get persistent database information
		users = shelve.open(self.filename,writeback=True)
		listing = users[muser].getlist()
		users.close()
		#decode information
		for i in range(len(listing)):
			listing[i][0] = vigenere.decode(mpass, listing[i][0])
			listing[i][1] = vigenere.decode(mpass, listing[i][1])
		#delete sentinel entry
		for i in range(len(listing)):
			if listing[i][0] == 'validuser':
				del listing[i]
				break
		return listing
		
	def validateUser(self,muser,mpass):
		'''validate a set of user credentials'''
		import shelve
		import vigenere
		#get persistent database information
		users = shelve.open(self.filename,writeback=True)
		if muser not in users:
			#new user
			users[muser] = childHashTable()
			users[muser].insert(vigenere.encode(mpass,'validuser'),vigenere.encode(mpass,'validated'))
			users.close()
			return True
		#existing user
		listing = users[muser].getlist()
		users.close()
		for i in range(len(listing)):
			listing[i][0] = vigenere.decode(mpass, listing[i][0])
			listing[i][1] = vigenere.decode(mpass, listing[i][1])
		for element in listing:
			if element[0] == 'validuser':
				#valid user confirmed
				return True
		#user validation unsuccessful
		return False
	
class childHashTable(object):
	
	def __init__(self):
		self.dictionary = []
		for i in range(128):
			self.dictionary.append([])
		self.cap = 128.0
		self.size = 0.0
		
	def insert(self,username,passwd):
		'''inserts a new username/password combination into the childHashTable'''
		import hashlib
		bucket = int((int(hashlib.md5(username).hexdigest(),16)>>100) % self.cap)
		steps = 0
		found = False
		
		#find the bucket
		if (len(self.dictionary[bucket]) == 0 or self.dictionary[bucket][0] == username):
			#found the bucket on the first try
			if len(self.dictionary[bucket]) == 0:
				#empty
				self.dictionary[bucket].append(username)
				self.dictionary[bucket].append(passwd)
			else:
				#not empty
				self.dictionary[bucket][1] = passwd
			found = True
			self.size = self.size + 1
		#didn't find the bucket on the first try
		if not found:
			#loop to find correct bucket
			while (len(self.dictionary[bucket]) != 0 and username not in self.dictionary[bucket] and steps != self.cap):
				bucket = int((bucket + 1) % self.cap)
				steps = steps + 1
			
			#insert the data
			if username in self.dictionary[bucket]:
				self.dictionary.bucket[1] == passwd
				found = True
			if not found:
				self.dictionary[bucket].append(username)
				self.dictionary[bucket].append(passwd)
				self.size = self.size + 1
		
		#check load factor
		if (self.size / self.cap) >= 0.9:
			#resize
			new_dictionary = []
			for i in range(int(self.cap)*2):
				new_dictionary.append([])
				new_cap = self.cap*2
			for i in range(int(self.cap)):
				if len(self.dictionary[i]) != 0:
					usr = self.dictionary[i][0]
					pwd = self.dictionary[i][1]
					bucket = int(int(hashlib.md5(username).hexdigest(),16) % new_cap)
					steps = 0
					found = False
		
					#find the bucket
					if (len(new_dictionary[bucket]) == 0 or new_dictionary[bucket][0] == usr):
						#found the bucket on the first try
						if len(self.dictionary[bucket]) == 0:
							#empty
							self.dictionary[bucket].append(username)
							self.dictionary[bucket].append(passwd)
						else:
							#not empty
							self.dictionary[bucket][1] = passwd
						found = True
					#didn't find the bucket on the first try
					if not found:
						#loop to find correct bucket
						while (len(new_dictionary[bucket]) != 0 and usr not in new_dictionary[bucket] and steps != new_cap):
							bucket = int((bucket + 1) % new_cap)
						#insert the data
						if usr in new_dictionary[bucket]:
							new_dictionary.bucket[1] == pwd
							found = True
						if not found:
							new_dictionary[bucket].append(usr)
							new_dictionary[bucket].append(pwd)
			self.dictionary = new_dictionary
			self.cap = new_cap
	
	def getlist(self):
		'''returns a listing of all username/password combinations in the table'''
		listing = []
		for i in range(int(self.cap)):
			if len(self.dictionary[i]) != 0:
				#only return blocks containing data
				listing.append(self.dictionary[i])
		return listing
