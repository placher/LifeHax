def shapwd(input):
	import hashlib
	from random import choice
	from string import punctuation
	hash = hashlib.sha256(input).hexdigest()
	pwd = ""
	for i in [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30]:
		num = int(hash[i:i+2],16)
		pwd = pwd + str(unichr(65 + num%57))
	pwd = list(pwd)
	for i in range(len(pwd)-1):
		if pwd[i] in punctuation:
			pwd[i] = str(choice(range(10)))
	return "".join(pwd)
